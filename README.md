# pgffigure style #

The `pgffigure` document class is designed for easier manipulation with plots and schemes produced by `tikz` and `pgfplots` packages. The following features are implemented:

1. `final|nofinal` option: disables or enables grid with 1 cm step in background of the document. 
1. `cyr|nocyr` option: allows to switch numbers view to Cyrillic notation (comma as decimal separator etc).
1. `\pgffopac` command is used for `opacity` property of nodes/figures make external images more transparent to show the background grid (is equivalent to `opacity = 0.5` in the `nofinal` mode and `opacity = 1` in the `final` mode).

This repository includes style file pgffigure.cls (should be placed to `~/texmf/tex/latex/pgffigure` or equivalent system path) and test.tex as minimal usage example.